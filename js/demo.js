function init7Seg(){
	console.log("init indruino meter");
	$("#indruino_meter_pv").sevenSeg({ digits: 5, value: 12.34 });
	$("#indruino_meter_sp").sevenSeg({ digits: 5, value: -56.78 });
}	

// =============== Function for chart demo ====================

var config = {
	type: 'line',
	data: {
		labels: [], // will be filled-out later
		datasets: [{
			label: "Setpoint",
			backgroundColor: window.chartColors.red,
			borderColor: window.chartColors.red,
			data: [], // will be filled-out later
			fill: false,
		}, {
			label: "Present value",
			fill: false,
			backgroundColor: window.chartColors.blue,
			borderColor: window.chartColors.blue,
			data: [], // will be filled-out later
		}]
	},
	options: {
		responsive: true,
		title:{
			display:true,
			text:'Thermal PID control'
		},
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		scales: {
			xAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Time'
				}
			}],
			yAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Temperature'
				}
			}]
		}
	}
};

function initChart(){
	console.log("init chart");
	var ctx = document.getElementById('demo-chart').getContext('2d');
	// fill out data set
	for (i=0;i<10;i++){
		config.data.labels.push(moment().add(i,'seconds').format('h:mm:ss'));
		config.data.datasets.forEach(function(dataset){
			dataset.data.push(randomScalingFactor());
		});
	}
	window.myLine = new Chart(ctx, config);
}

document.getElementById('randomizeData').addEventListener('click', function() {
	config.data.datasets.forEach(function(dataset) {
		dataset.data = dataset.data.map(function() {
			return randomScalingFactor();
		});

	});

	window.myLine.update();
});

var colorNames = Object.keys(window.chartColors);

function remove_past_data(){
	if (config.data.labels.length > 10){
		config.data.labels.shift();
		config.data.datasets[0].data.shift();
		config.data.datasets[1].data.shift();
	}
}

document.getElementById('addData').addEventListener('click', function() {
	config.data.labels.push(moment().format('hh:mm:ss'));
	config.data.datasets.forEach(function(dataset) {
		dataset.data.push(randomScalingFactor());
	});

	remove_past_data();

	window.myLine.update();

	var sp = config.data.datasets[0].data[config.data.datasets[0].data.length - 1];
	var pv = config.data.datasets[1].data[config.data.datasets[1].data.length - 1];
	$("#indruino_meter_sp").sevenSeg({ value: sp });
	$("#indruino_meter_pv").sevenSeg({ value: pv});
});

document.getElementById('removeData').addEventListener('click', function() {
	config.data.labels.splice(-1, 1); // remove the label first

	config.data.datasets.forEach(function(dataset, datasetIndex) {
		dataset.data.pop();
	});

	window.myLine.update();
});

function interval_check(){
	$.get("http://10.86.85.142", function(data, status){
		console.log(data);

		config.data.labels.push(moment().format('hh:mm:ss'));
		config.data.datasets[0].data.push(data.sp);
		config.data.datasets[1].data.push(data.pv);

		remove_past_data();

		window.myLine.update();

		$("#indruino_meter_sp").sevenSeg({ value: data.sp });
		$("#indruino_meter_pv").sevenSeg({ value: data.pv});
	});
}

$(document).ready(function() {

	// init components
	init7Seg();
	initChart();

	// Update chart each 1s
	setInterval(interval_check, 1000);
});
